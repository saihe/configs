" Set preferred shell
set shell=/usr/local/bin/zsh

" Automatically install vim-plug
if empty(glob('~/.vim/autoload/plug.vim'))
    silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

" Load plugins
call plug#begin()
Plug 'w0rp/ale'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'itchyny/lightline.vim'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
Plug 'joshdick/onedark.vim'
Plug 'rust-lang/rust.vim'
Plug 'dag/vim-fish'
Plug 'airblade/vim-gitgutter'
Plug 'machakann/vim-highlightedyank'
Plug 'jeffkreeftmeijer/vim-numbertoggle'
Plug 'airblade/vim-rooter'
Plug 'cespare/vim-toml'
call plug#end()

" Show line numbers and related info
set cursorline
set number
set relativenumber
set ruler
set showcmd
set signcolumn=yes

" Indentation and tab settings
filetype plugin indent on
set autoindent
set cindent
set cinoptions=g2,h2
set expandtab
set shiftwidth=4
set softtabstop=4

" Use `g` flag by default with :s/foo/bar/
set gdefault

" Case-insensitive searching unless capital letters are used
set ignorecase
set smartcase

" Alter timeout and redraw
set lazyredraw
set timeoutlen=300
set ttyfast
set updatetime=100

" Splits
set splitbelow
set splitright

" Show next 2 lines and next 5 columns when scrolling
set scrolloff=2
set sidescrolloff=5

" Command-line completions
set wildmenu
set wildmode=full

" Improvements to undoing
set undodir=~/.config/nvim/undo
set undofile

" Use the One Dark color scheme
set termguicolors
let g:onedark_hide_endofbuffer = 1
let g:onedark_terminal_italics = 1
colorscheme onedark

" Status bar
set noshowmode
let g:lightline = {
    \ 'colorscheme': 'onedark',
    \ 'active': {
    \     'left': [
    \         ['mode', 'paste'],
    \         ['filename'],
    \     ],
    \ },
    \ 'component_function': {
    \     'filename': 'LightlineFilename',
    \ },
    \ 'separator': {
    \     'left': "\ue0b0",
    \     'right': "\ue0b2",
    \ },
    \ }

function! LightlineFilename() abort
    let filename = expand('%:t') !=# '' ? expand('%:t') : '[No Name]'
    let modified = &modified ? ' +' : ''
    return filename . modified
endfunction

" Highlight trailing whitespace
highlight ExtraWhitespace ctermbg=red guibg=red
match ExtraWhitespace /\s\+$/
augroup HighlightWhitespace
    autocmd!
    autocmd BufWinEnter * match ExtraWhitespace /\s\+$/
    autocmd InsertEnter * match ExtraWhitespace /\s\+\%#\@<!$/
    autocmd InsertLeave * match ExtraWhitespace /\s\+$/
    autocmd BufWinLeave * call clearmatches()
augroup END

" Yank highlighting
let g:highlightedyank_highlight_duration = -1

" Ale configuration
let g:ale_fix_on_save = 1
let g:ale_fixers = {
    \ '*': ['remove_trailing_lines', 'trim_whitespace'],
    \ 'rust': ['rustfmt'],
    \ }
let g:ale_lint_on_text_changed = 'normal'
let g:ale_lint_on_insert_leave = 1
let g:ale_lint_on_enter = 1
let g:ale_linters = {
    \ 'rust': ['rls'],
    \ }
let g:ale_sign_error = "\u2716"
let g:ale_sign_warning = "\u26a0"
let g:ale_sign_info = 'i'
let g:ale_sign_hint = "\u27a4"
let g:ale_rust_rls_config = {
    \ 'rust': {
    \     'all_targets': 1,
    \     'build_on_save': 1,
    \     'clippy_preference': 'on',
    \ },
    \ }
let g:ale_virtualtext_cursor = 1

" Fish filetype configuration
augroup Fish
    autocmd!
    autocmd FileType fish compiler fish
augroup END

" Leader key
let mapleader = ' '

" Mouse support
set mouse=a

" Allow ; as :
nnoremap ; :

" Prevent use of arrow keys in order to force HJKL
nnoremap <Up> <Nop>
nnoremap <Down> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>
inoremap <Left> <Nop>
inoremap <Right> <Nop>

" More intuitive split navigation with HJKL
nnoremap <C-h> <C-w><C-h>
nnoremap <C-j> <C-w><C-j>
nnoremap <C-k> <C-w><C-k>
nnoremap <C-l> <C-w><C-l>

" Quicksave hotkey
nnoremap <Leader>w :w<CR>

" Switch buffers quickly with left and right arrow keys
nnoremap <Left> :bp<CR>
nnoremap <Right> :bn<CR>

" Switch to last buffer
nnoremap <Leader><Leader> <C-^>

" Open NERDTree
nnoremap <C-n> :NERDTreeToggle<CR>

" Ale keymappings
nnoremap <Leader>f :ALEFix<CR>
nnoremap <silent>gd :ALEGoToDefinition<CR>
